import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import ElementUI from 'element-ui';
import './styles/element-style/theme/index.css';
// medium / small / mini

Vue.use(ElementUI, { size: 'small' });

Vue.config.productionTip = false
// 重置样式
import "./styles/reset.css"
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
