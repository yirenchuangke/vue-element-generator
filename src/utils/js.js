

import ruleTrigger from './ruleTrigger'
export function makeUpJs(data, formConfig) {


  let rules = rulesData(data)
  let options = optionsData(data)
  let datas = ` data() {
  return {
    formData:{${formData(data)}},
    ${rules}
    ${options}
  }
},`
  return `

export default {
   ${datas}
    
}
`
}


function optionsData(data) {
  let options = []
  data.forEach(element => {
    if (element.slot && element.slot.options) {
      options.push(`${element.model}Options:${JSON.stringify(element.slot.options)}`)
    }
  });
  let optionsStr = options.join(",")
  return optionsStr
}


function rulesData(data) {
  let rules = []
  data.forEach(element => {
    if (element.rules.length > 0) {
      rules.push(`${element.model}:${buildRules(element)}`)

    }
  });
  let rulesStr = ""
  if (rules.length > 0) {
    rulesStr = `rules:{${rules.join(",")}},`
  }
  return rulesStr
}

function buildRules(data) {
  let rules = []
  data.rules.forEach(element => {
    if (Object.keys(element).includes('required')) {
      rules.push(
        `{required:${data.attributes.required},message:"${element.message}",trigger:"${ruleTrigger[data.tag]}"}`
      )
    } else {
      if (element.pattern && element.message) {
        rules.push(
          `{pattern:${element.pattern},message:"${element.message}",trigger:"${ruleTrigger[data.tag]}"}`
        )
      }

    }
  });


  let rulesStr = ""
  if (rules.length > 0) {
    rulesStr = `[${rules}]`
  }
  return rulesStr
}
function formData(data) {
  let formDatas = []

  data.forEach(element => {

    formDatas.push(`${element.model}:${JSON.stringify(element.defaultValue)}`)

  });
  return formDatas.join(",")
}