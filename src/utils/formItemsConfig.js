// 基础控件
export const basicsList = [
  {
    id: 1,
    type: "input", // 表单类型
    label: "输入框", // 标题文字
    tag: "el-input",
    style: { width: "100%" },
    defaultValue: "",
    document: "https://element.eleme.cn/#/zh-CN/component/input",
    attributes: {
      required: true, // 是否必填
      span: 24, // 后期24分栏
      clearable: true, //是否可清空
      placeholder: "请输入", // 输入框占位文本
      disabled: false, // 是否禁用
      maxlength: null, // 最大输入长度
      "show-word-limit": false, // 是否显示输入字数统计
      readonly: false, // 是否只读
      "prefix-icon": "", // 输入框头部图标
      "suffix-icon": "", // 输入框尾部图标
      "show-password": false // 是否显示切换密码图标
    },
    slot: {
      prepend: "", // 输入框前置内容
      append: "" // 输入框后置内容
    },
    rules: [
      //验证规则
      {
        required: true, // 必须填写
        message: "必填项"
      }
    ]
  },
  {
    id: 2,
    type: "textarea", // 表单类型
    label: "文本框", // 标题文字
    tag: "el-input",
    style: { width: "100%" },
    defaultValue: "",
    document: "https://element.eleme.cn/#/zh-CN/component/input",
    attributes: {
      required: true, // 是否必填
      span: 24, // 后期24分栏
      clearable: true, //是否可清空
      placeholder: "请输入", // 输入框占位文本
      disabled: false, // 是否禁用
      maxlength: null, // 最大输入长度
      "show-word-limit": false, // 是否显示输入字数统计
      readonly: false, // 是否只读
      "prefix-icon": "", // 输入框头部图标
      "suffix-icon": "", // 输入框尾部图标
      // 自适应内容高度
      autosize: {
        minRows: 4,
        maxRows: 4
      },
      "show-password": false // 是否显示切换密码图标
    },
    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  },
  {
    id: 3,
    type: "number", // 表单类型
    label: "数字输入框", // 标题文字
    tag: "el-input",
    style: { width: "100%" },
    defaultValue: null,
    document: "https://element.eleme.cn/#/zh-CN/component/input",
    attributes: {
      required: true, // 是否必填
      span: 24, // 后期24分栏
      clearable: true, //是否可清空
      placeholder: "请输入", // 输入框占位文本
      disabled: false, // 是否禁用
      maxlength: null, // 最大输入长度
      "show-word-limit": false, // 是否显示输入字数统计
      readonly: false, // 是否只读
      "prefix-icon": "", // 输入框头部图标
      "suffix-icon": "", // 输入框尾部图标
      "show-password": false // 是否显示切换密码图标
    },
    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  },
  {
    id: 4,
    type: "select", // 表单类型
    label: "下拉选择器", // 标题文字
    tag: "el-select",
    style: { width: "100%" },
    defaultValue: null,
    document: "https://element.eleme.cn/#/zh-CN/component/select",
    attributes: {
      required: true, // 是否必填
      span: 24, // 后期24分栏
      clearable: true, //是否可清空
      placeholder: "请输入", // 输入框占位文本
      disabled: false, // 是否禁用
      filterable: false, // 是否可搜索
      multiple: false // 是否多选
    },
    slot: {
      options: [
        // 下拉选择项配置
        {
          value: "1",
          label: "下拉框1"
        },
        {
          value: "2",
          label: "下拉框2"
        }
      ]
    },

    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  },
  {
    id: 5,
    type: "checkbox",
    label: "多选框",
    tag: "el-checkbox-group",
    style: { width: "100%" },
    defaultValue: [],
    document: "https://element.eleme.cn/#/zh-CN/component/checkbox",
    attributes: {
      required: true, // 是否必填
      span: 24, // 后期24分栏
      disabled: false, //是否禁用
      size: "medium", // 	Checkbox 的尺寸	medium / small / mini
      min: null, // 可被勾选的 checkbox 的最小数量
      max: null, // 可被勾选的 checkbox 的最大数量
      optionType: "default", // 多选框类型 checkbox-默认 / el-checkbox-button-按钮样式
      border: false // 是否显示边框
    },
    slot: {
      options: [
        // 下拉选择项配置
        {
          value: "1",
          label: "下拉框1"
        },
        {
          value: "2",
          label: "下拉框2"
        }
      ]
    },
    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  },
  {
    id: 6,
    type: "radio", // 表单类型
    label: "单选框", // 标题文字
    tag: "el-radio-group",
    style: { width: "100%" },
    defaultValue: null,
    document: "https://element.eleme.cn/#/zh-CN/component/radio",
    attributes: {
      required: true, // 是否必填
      span: 24, // 后期24分栏
      disabled: false, //是否禁用
      size: "medium", // 	Checkbox 的尺寸	medium / small / mini
      optionType: "default", // 多选框类型 checkbox-默认 / el-checkbox-button-按钮样式
      border: false // 是否显示边框
    },
    slot: {
      options: [
        {
          value: "1",
          label: "选项1"
        },
        {
          value: "2",
          label: "选项2"
        },
        {
          value: "3",
          label: "选项3"
        }
      ]
    },

    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  },
  {
    id: 7,
    type: "date", // 表单类型
    label: "日期选择", // 标题文字
    tag: "el-date-picker",
    style: { width: "100%" },
    defaultValue: "",
    document: "https://element.eleme.cn/#/zh-CN/component/date-picker",
    attributes: {
      required: true, // 是否必填
      span: 24, // 后期24分栏
      placeholder: "请选择", // 非范围选择时的占位内容
      type: "date", // 显示类型 year/month/date/dates/ week/datetime/datetimerange/ daterange/monthrange
      disabled: false, // 是否禁用
      clearable: true, // 是否清除
      format: "yyyy-MM-dd", // 时间格式化(TimePicker)
      "value-format": "yyyy-MM-dd", // 绑定值的格式
      readonly: false // 完全只读
    },

    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  },
  {
    id: 8,
    type: "daterange", // 表单类型
    label: "日期范围", // 标题文字
    tag: "el-date-picker",
    style: { width: "100%" },
    defaultValue: [],
    document: "https://element.eleme.cn/#/zh-CN/component/date-picker",
    attributes: {
      required: true, // 是否必填
      span: 24, // 后期24分栏
      type: "daterange", // 显示类型 year/month/date/dates/ week/datetime/datetimerange/ daterange/monthrange
      "range-separator": "至", // 选择范围时的分隔符
      "start-placeholder": "开始日期", // 范围选择时开始日期的占位内容
      "end-placeholder": "结束日期", // 范围选择时结束日期的占位内容
      disabled: false, // 是否禁用
      clearable: true, // 是否清除
      format: "yyyy-MM-dd", // 时间格式化(TimePicker)
      "value-format": "yyyy-MM-dd", // 绑定值的格式
      readonly: false // 完全只读
    },

    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  },
  {
    id: 9,
    type: "time", // 表单类型
    label: "时间选择", // 标题文字
    tag: "el-time-picker",
    style: { width: "100%" },
    defaultValue: "",
    document: "https://element.eleme.cn/#/zh-CN/component/time-picker",
    attributes: {
      required: true, // 是否必填
      span: 24, // 后期24分栏
      placeholder: "请选择",
      disabled: false, // 是否禁用
      clearable: true, // 是否清除
      "picker-options": {
        // 当前时间日期选择器特有的选项参考下表
        selectableRange: "00:00:00-23:59:59"
      },
      format: "HH:mm:ss", // 时间格式化(TimePicker)
      "value-format": "HH:mm:ss" // 绑定值的格式
    },

    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  },
  {
    id: 10,
    type: "time", // 表单类型
    label: "时间范围", // 标题文字
    tag: "el-time-picker",
    style: { width: "100%" },
    defaultValue: [],
    document: "https://element.eleme.cn/#/zh-CN/component/time-picker",
    attributes: {
      required: true, // 是否必填
      span: 24, // 后期24分栏
      disabled: false, // 是否禁用
      clearable: true, // 是否显示清除按钮
      "is-range": true, // 是否为时间范围选择
      "range-separator": "至", // 选择范围时的分隔符
      "start-placeholder": "开始时间", // 范围选择时开始日期的占位内容
      "end-placeholder": "结束时间", // 范围选择时开始日期的占位内容
      format: "HH:mm:ss", // 时间格式化(TimePicker)
      "value-format": "HH:mm:ss" // 绑定值的格式
    },

    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  },
  {
    id: 11,
    type: "rate", // 表单类型
    label: "评分", // 标题文字
    tag: "el-rate",
    style: { width: "100%" },
    document: "https://element.eleme.cn/#/zh-CN/component/rate",
    defaultValue: null,
    attributes: {
      required: true, // 是否必填
      span: 24, // 后期24分栏
      disabled: false, // 是否禁用
      "allow-half": false, // 是否允许半选
      "show-text": false, // 是否显示辅助文字
      "show-score": false // 是否显示当前分数，show-score 和 show-text 不能同时为真
    },

    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  },
  {
    id: 12,
    type: "slider", // 表单类型
    label: "滑动输入条", // 标题文字
    tag: "el-slider",
    defaultValue: null,
    document: "https://element.eleme.cn/#/zh-CN/component/slider",
    attributes: {
      required: true, // 是否必填
      span: 24, // 后期24分栏
      disabled: false, // 是否禁用
      min: 0, // 最小值
      max: 100, // 最大值
      step: 1, // 步长
      "show-stops": false, // 是否显示间断点
      range: false // 是否为范围选择
    },

    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  },
  // {
    // id: 13,
  //   type: "uploadFile", // 表单类型
  //   label: "上传文件", // 标题文字

  //   attributes: {
  // required: true, // 是否必填
  // span: 24, // 后期24分栏
  //     defaultValue: "[]",
  //     multiple: false,
  //     disabled: false, // 是否禁用
  //     hidden: false, // 是否隐藏，false显示，true隐藏
  //     drag: false,
  //     downloadWay: "a",
  //     dynamicFun: "",
  //     width: "100%",
  //     limit: 3,
  //     data: "{}",
  //     fileName: "file",
  //     headers: {},
  //     action: "http://cdn.kcz66.com/uploadFile.txt",
  //     placeholder: "上传"
  //   },
  //

  //   rules: [
  //     {
  //       required: false,
  //       message: "必填项"
  //     }
  //   ]
  // },
  // {
    // id: 14,
  //   type: "uploadImg",
  //   label: "上传图片",

  //   attributes: {
  // required: true, // 是否必填
  // span: 24, // 后期24分栏
  //     defaultValue: "[]",
  //     multiple: false,
  //     hidden: false, // 是否隐藏，false显示，true隐藏
  //     disabled: false, // 是否禁用
  //     width: "100%",
  //     data: "{}",
  //     limit: 3,
  //     placeholder: "上传",
  //     fileName: "image",
  //     headers: {},
  //     action: "http://cdn.kcz66.com/upload-img.txt",
  //     listType: "picture-card"
  //   },
  //

  //   rules: [
  //     {
  //       required: false,
  //       message: "必填项"
  //     }
  //   ]
  // },
  // {
    // id: 15,
  //   type: "treeSelect", // 表单类型
  //   label: "树选择器", // 标题文字

  //   attributes: {
  // required: true, // 是否必填
  // span: 24, // 后期24分栏
  //     disabled: false, //是否禁用
  //     defaultValue: undefined, // 默认值
  //     multiple: false,
  //     hidden: false, // 是否隐藏，false显示，true隐藏
  //     clearable: false, // 是否显示清除按钮
  //     showSearch: false, // 是否显示搜索框，搜索选择的项的值，而不是文字
  //     treeCheckable: false,
  //     placeholder: "请选择",
  //     dynamicKey: "",
  //     dynamic: true,
  //
  //   },
  //
  // slot: {
  //   options: [
  //     {
  //       value: "1",
  //       label: "选项1"
  //     },
  //     {
  //       value: "2",
  //       label: "选项2"
  //     },
  //     {
  //       value: "3",
  //       label: "选项3"
  //     }
  //   ]
  // },
  //   rules: [
  //     {
  //       required: false,
  //       message: "必填项"
  //     }
  //   ]
  // },
  // {
    // id: 16,
  //   type: "cascader", // 表单类型
  //   label: "级联选择器", // 标题文字
  //   tag: 'el-cascader',
  //   style: { width: '100%' },
  //   defaultValue: [],
  //   attributes: {
  //     required: true, // 是否必填
  //     span: 24, // 后期24分栏
  //     disabled: false, //是否禁用
  //     placeholder: '请选择',
  //     props: {
  //       props: {
  //         multiple: false,
  //         label: 'label',
  //         value: 'value',
  //         children: 'children'
  //       }
  //     },
  //     'show-all-levels': true,
  //     disabled: false, // 是否禁用
  //     clearable: true,
  //     filterable: false,
  //     separator: '/',

  //   },

  //   slot: {
  //     options: [
  //       {
  //         value: "1",
  //         label: "选项1"
  //       },
  //       {
  //         value: "2",
  //         label: "选项2"
  //       },
  //       {
  //         value: "3",
  //         label: "选项3"
  //       }
  //     ]
  //   },
  //   rules: [
  //     {
  //       required: false,
  //       message: "必填项"
  //     }
  //   ]
  // },

  // {
    // id: 17,
  //   type: "editor",
  //   label: "富文本",

  //   list: [],
  //   attributes: {
  // required: true, // 是否必填
  // span: 24, // 后期24分栏
  //     height: 300,
  //     placeholder: "请输入",
  //     defaultValue: "",
  //     chinesization: true,
  //     hidden: false, // 是否隐藏，false显示，true隐藏
  //     disabled: false, // 是否禁用
  //     showLabel: false,
  //     width: "100%"
  //   },
  //

  //   rules: [
  //     {
  //       required: false,
  //       message: "必填项"
  //     }
  //   ]
  // },
  {
    id:20,
    type: "switch", // 表单类型
    label: "开关", // 标题文字
    tag: "el-switch",
    defaultValue: null,
    document: "https://element.eleme.cn/#/zh-CN/component/switch",
    attributes: {
      required: true, // 是否必填
      span: 24, // 后期24分栏
      disabled: false, //是否禁用
      "active-text": "", // switch 打开时的文字描述
      "inactive-text": "", // switch 关闭时的文字描述
      "active-color": null, // switch 打开时的背景色
      "inactive-color": null, // switch 关闭时的背景色
      "active-value": true, // 	switch 打开时的值
      "inactive-value": false // switch 关闭时的值
    },

    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  }
  // {
    // id: 18,
  //   type: "button", // 表单类型
  //   label: "按钮", // 标题文字

  //   attributes: {
  // required: true, // 是否必填
  // span: 24, // 后期24分栏
  //     type: "primary",
  //     handle: "submit",
  //     dynamicFun: "",
  //     hidden: false, // 是否隐藏，false显示，true隐藏
  //     disabled: false // 是否禁用，false不禁用，true禁用
  //   },
  //   key: ""
  // },
  // {
  //   type: "alert",
  //   label: "警告提示",
  //   attributes: {
  // required: true, // 是否必填
  // span: 24, // 后期24分栏
  //     type: "success",
  //     description: "",
  //     showIcon: false,
  //     banner: false,
  //     hidden: false, // 是否隐藏，false显示，true隐藏
  //     closable: false
  //   },
  //   key: ""
  // },
  // {
    // id: 19,
  //   type: "text",
  //   label: "文字",
  //   attributes: {
  // required: true, // 是否必填
  // span: 24, // 后期24分栏
  //     textAlign: "left",
  //     hidden: false, // 是否隐藏，false显示，true隐藏
  //     showRequiredMark: false
  //   },
  //   key: ""
  // },
  // {
  //   type: "html",
  //   label: "HTML",
  //   attributes: {
  // required: true, // 是否必填
  // span: 24, // 后期24分栏
  //     hidden: false, // 是否隐藏，false显示，true隐藏
  //     defaultValue: "<strong>HTML</strong>"
  //   },
  //   key: ""
  // }
];
// 表单属性【右面板】
export const formConfig = {
  formRef: "elForm",
  formModel: "formData",
  size: "medium",
  labelPosition: "right",
  labelWidth: 100,
  formRules: "rules",
  gutter: 15,
  disabled: false, // 是否禁用
  span: 24,
  formBtns: false
};
