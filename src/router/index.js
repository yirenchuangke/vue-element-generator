import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import(/* webpackChunkName: "about" */ '@/pages/index')
  },
  {
    path: '/form',
    name: 'FormGenerator',
    component: () => import(/* webpackChunkName: "about" */ '@/pages/FormGenerator')
  },
  {
    path: '/echarts',
    name: 'Echarts',
    component: () => import(/* webpackChunkName: "about" */ '@/pages/Echarts')
  },
  {
    path: '/page',
    name: 'PageGenerator',
    component: () => import(/* webpackChunkName: "about" */ '@/pages/PageGenerator')
  },
  {
    path: '/myForm',
    name: 'MyForm',
    component: () => import(/* webpackChunkName: "about" */ '@/pages/MyForm')
  }

]

const router = new VueRouter({
  base: process.env.BASE_URL,
  routes
})

export default router
