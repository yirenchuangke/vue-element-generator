export const echartsList = [

  {
    name: "折线图",
    type: "line"
  },
  {
    name: "柱状图",
    type: "bar"
  },
  {
    name: "饼图",
    type: "pie"
  },
  {
    name: "散点图",
    type: "scatter"
  },
  {
    name: "雷达图",
    type: "radar"
  },
  {
    name: "盒须图",
    type: "boxplot"
  },
  {
    name: "热力图",
    type: "heatmap"
  },
  {
    name: "关系图",
    type: "graph"
  },
  {
    name: "旭日图",
    type: "sunburst"
  },
  {
    name: "桑基图",
    type: "sankey"
  }, {
    name: "漏斗图",
    type: "funnel"
  },
  {
    name: "仪表盘",
    type: "gauge"
  },
  {
    name: "象形柱图",
    type: "pictorialBar"
  }
]