export default {
  fields: {
    name: {
      type: "input", // 表单类型
      label: "标题文字", // 标题文字
      model: "name",
      attributes: {
        type: "text",
        width: "100%", // 宽度
        defaultValue: "", // 默认值
        placeholder: "请输入标题文字", // 没有输入时，提示文字
        clearable: false,
        disabled: false // 是否禁用，false不禁用，true禁用
      },
      rules: [
        //验证规则
        {
          required: false, // 必须填写
          message: "必填项"
        }
      ]
    },
    names: {
      type: "input", // 表单类型
      label: "标题文字", // 标题文字
      model: "names",
      attributes: {
        type: "text",
        width: "100%", // 宽度
        defaultValue: "", // 默认值
        placeholder: "请输入标题文字", // 没有输入时，提示文字
        clearable: false,
        maxLength: null,
        hidden: false, // 是否隐藏，false显示，true隐藏
        disabled: false // 是否禁用，false不禁用，true禁用
      },
      rules: [
        //验证规则
        {
          required: false, // 必须填写
          message: "必填项"
        }
      ]
    },
  },
  "config": {
    "labelPosition": "right",
    "labelCol": {
      "xs": 4,
      "sm": 4,
      "md": 4,
      "lg": 4,
      "xl": 4,
      "xxl": 4
    },
    "wrapperCol": {
      "xs": 18,
      "sm": 18,
      "md": 18,
      "lg": 18,
      "xl": 18,
      "xxl": 18
    },
    "hideRequiredMark": false,
    "customStyle": ""
  }
}